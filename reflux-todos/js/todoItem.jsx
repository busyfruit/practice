import React, {Component} from 'react'
import classNames from 'classnames'
import TodoActions from './action.js'
import {decorate as mixin} from 'react-mixin'

@mixin(TodoActions)
export default class TodoItem extends Component {

    constructor (props) {
        super(props)
        this.state = {
            editing: false
        }
        this.data = this.props.data
    }
    
    onToggle () {
        this.toggleItem(this.data.id)
    }

    onDestroy () {
        this.removeItem(this.data.id)
    }

    onEditing () {
        this.setState({editing: true}, () => this.refs.editField.focus())
    }

    onKeyDown (e) {
        if (e.which === 27) {
            this.setState({
                editing: false
            })
        }
        else if (e.which === 13) {
            this.setState({
                editing: false
            })
            let id = this.data.id
            let text = e.target.value
            this.updateItem({id, text})
        }
    }

    onBlur () {
        this.setState({
            editing: false
        })
    }

    render () {
        return (
            <li className={classNames({
                completed: this.data.completed,
                editing: this.state.editing
            })}>
                <div className="view">
                    <input
                        className="toggle"
                        onChange={this.onToggle.bind(this)}
                        checked={this.data.completed}
                        type="checkbox"
                    />
                    <label
                        onDoubleClick={this.onEditing.bind(this)}
                    >
                        {this.data.text}
                    </label>
                    <button
                        className="destroy"
                        onClick={this.onDestroy.bind(this)}
                    >
                    </button>
                </div>
                <input
                    className="edit"
                    ref="editField"
                    type="text"
                    defaultValue={this.data.text}
                    onKeyDown={this.onKeyDown.bind(this)}
                    onBlur={this.onBlur.bind(this)}
                />
            </li>
        )
    }
}
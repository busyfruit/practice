// 示例1
import {Dialog, DialogWithBorder, DialogWithScrollbar} from './decorator-pattern'

let dialogWidthBorderAndScrollbar = new DialogWithScrollbar(new DialogWithBorder(new Dialog()))
dialogWidthBorderAndScrollbar.render()
dialogWidthBorderAndScrollbar.close()

require('../css/index.css')

import React from 'react'
import ReactDOM from 'react-dom'
import {LazyloadPanel} from './hoc.js'

ReactDOM.render(<LazyloadPanel data="test data" />, document.querySelector('.container'))



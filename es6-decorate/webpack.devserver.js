var webpackConfig = require('./webpack.dev.js');
var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var chalk = require('chalk');

var compiler = webpack(webpackConfig);

var server = new WebpackDevServer(compiler, {
    hot: true,
    publicPath: '/static/',
});

server.listen(8888, 'localhost', function () {
    console.log(chalk.red('提示:'), chalk.green('使用Ctrl+C退出服务器!'));
});

process.on('SIGINT', function () {
    process.exit();
});

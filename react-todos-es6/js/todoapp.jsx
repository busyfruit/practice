import React, {Component} from 'react'
import Header from './header.jsx'
import TodoItem from './todoItem.jsx'
import Toolbar from './toolbar.jsx'
import {ALL_FILTER, ACTIVE_FILTER, COMPLETED_FILTER} from './config.js'

export default class TodoApp extends Component {

    constructor (props) {
        super(props)
        this.store = this.props.store
        this.state = {
            filter: ALL_FILTER
        }
    }
    
    addItem (text) {
        this.store.insert(text)
    }
    
    toggleItem (id, complete) {
        this.store.update({
            id: id,
            complete: complete
        })
    }

    toggleAll (checked) {
        let itemList = this.store.read('complete', !checked)
        itemList.forEach((item) => {
            item.complete = checked
            this.store.update(item)
        })
    }

    destroyItem (id) {
        this.store.remove('id', id)
    }

    updateItem (id, text) {
        this.store.update({
            id: id,
            text: text
        })
    }

    clearCompletedItems () {
        let completedItems = this.store.read('complete', true)
        let ids = completedItems.map((item) => item.id)
        this.store.remove('id', ...ids)
    }

    getTodoList () {
        let todoList;

        switch (this.state.filter) {
            case ALL_FILTER:
                todoList = this.store.read()
                break
            case COMPLETED_FILTER:
                todoList = this.store.read('complete', true)
                break
            case ACTIVE_FILTER:
                todoList = this.store.read('complete', false)
                break
            default:
                todoList = []
                break
        }

        return todoList.map((item) => {
            let stylesheet = this.props.stylesheet
            return (
                <TodoItem
                    key={item.id}
                    data={item}
                    handleToggle={this.toggleItem.bind(this)}
                    handleDestroy={this.destroyItem.bind(this)}
                    handleUpdate={this.updateItem.bind(this)}
                    stylesheet={stylesheet}
                />
            )
        })
    }
    
    render () {
        let todoList = this.getTodoList()
        let remainItemsCount = this.store.read('complete', false).length
        let completedItemsCount = this.store.read('complete', true).length
        let stylesheet = this.props.stylesheet

        return (
            <div className={stylesheet.todoapp}>
                <Header
                    remainItemsCount={remainItemsCount}
                    handleSubmit={this.addItem.bind(this)}
                    handleToggleAll={this.toggleAll.bind(this)}
                    stylesheet={stylesheet}
                />
                <ul className={stylesheet['todo-list']}>
                    {todoList}
                </ul>
                <Toolbar
                    completedItemsCount={completedItemsCount}
                    remainItemsCount={remainItemsCount}
                    filter={this.state.filter}
                    onClear={this.clearCompletedItems.bind(this)}
                    stylesheet={stylesheet}
                />
            </div>
        )
    }
}
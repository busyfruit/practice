import React, {Component} from 'react'
import classNames from 'classnames/bind'
import {ALL_FILTER, ACTIVE_FILTER, COMPLETED_FILTER} from './config.js'

export default class Toolbar extends Component{

    render () {

        let clearBtn = null
        let stylesheet=this.props.stylesheet

        // classNames的bind版本，支持css modules，正确映射未编译的classname和编译后的classname
        let cx = classNames.bind(stylesheet)

        if (this.props.completedItemsCount) {
            clearBtn = (
                <button
                    className={stylesheet['clear-completed']}
                    onClick={this.props.onClear}
                >Clear completed</button>
            )
        }

        return (
            <footer className={stylesheet.footer}>
                <span className={stylesheet['todo-count']}>
                    <strong>{this.props.remainItemsCount}</strong>
                    <span> </span>
                    <span>items </span>
                    <span>left</span>
                </span>
                <ul className={stylesheet.filters}>
                    <li>
                        <a
                            className={cx({selected: this.props.filter === ALL_FILTER})}
                            href="/"
                        >All</a>
                    </li>
                    <li>
                        <a
                            className={cx({selected: this.props.filter === ACTIVE_FILTER})}
                            href="/active"
                        >Active</a>
                    </li>
                    <li>
                        <a
                            className={cx({selected: this.props.filter === COMPLETED_FILTER})}
                            href="/completed"
                        >Completed</a>
                    </li>
                </ul>
                {clearBtn}
            </footer>
        )
    }

}

import stylesheet from '../css/index.css'
console.log(stylesheet)

import React from 'react'
import ReactDOM from 'react-dom'
import page from 'page'
import Store from './store.js'
import TodoApp from './todoapp.jsx'
import {ALL_FILTER, ACTIVE_FILTER, COMPLETED_FILTER} from './config.js'

let store = new Store('todos')
let container = document.querySelector('#todoApp')
let todoApp = render()

store.subscribe(render)

function render() {
    return ReactDOM.render(<TodoApp store={store} stylesheet={stylesheet} />, container)
}

page('/', () => {
    todoApp.setState({
        filter: ALL_FILTER
    })
})

page('/active', () => {
    todoApp.setState({
        filter: ACTIVE_FILTER
    })
})

page('/completed', () => {
    todoApp.setState({
        filter: COMPLETED_FILTER
    })
})

page.start()


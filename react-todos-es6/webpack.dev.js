module.exports = {
    entry: {
        aio: './js/index.jsx'
    },

    output: {
        path: __dirname + '/.output',
        filename: '[name].js',
        publicPath: '/static'
    },

    module: {
        loaders: [
            {
                test: /\.css/,
                /**
                * 添加?module默认开启scope css
                * localIentName定制生成的classname格式
                */
                loaders: ['style', 'css?module&localIdentName=[name]--[local]--[hash:base64:5]']
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel'
            }
        ]
    }
};
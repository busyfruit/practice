"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
* 数据结构：列表
*/

var List = function () {
    function List() {
        _classCallCheck(this, List);

        this._datastore = [];
        this._size = 0;
        this._point = 0;
    }

    _createClass(List, [{
        key: "append",
        value: function append(item) {
            this._size++;
            this._datastore.push(item);
        }
    }, {
        key: "insert",
        value: function insert(item, pos) {
            this._size++;
            this._datastore.splice(pos, 0, item);
        }
    }, {
        key: "remove",
        value: function remove(item) {
            var index = this._datastore.indexOf(item);
            if (index !== -1) {
                this._datastore.splice(index, 1);
                this._size--;
            }
        }
    }, {
        key: "length",
        value: function length() {
            return this._size;
        }
    }, {
        key: "next",
        value: function next() {
            var item = this._datastore[this._point];
            var done = false;
            if (this._point >= this._size) {
                done = true;
            } else {
                this._point++;
                done = false;
            }

            return {
                done: done,
                value: item
            };
        }
    }, {
        key: "prev",
        value: function prev() {
            if (this._point > 0) {
                this._point--;
            }
        }
    }, {
        key: "moveTo",
        value: function moveTo(pos) {
            this._point = pos;
        }
    }, {
        key: "getElement",
        value: function getElement() {
            return this._datastore[this._point];
        }
    }, {
        key: "getPoint",
        value: function getPoint() {
            return this._point;
        }
    }, {
        key: "resetPoint",
        value: function resetPoint() {
            this._point = 0;
        }
    }, {
        key: "clear",
        value: function clear() {
            this._datastore = [];
            this._point = 0;
            this._size = 0;
        }

        // 部署ES6 Iterator接口

    }, {
        key: Symbol.iterator,
        value: function value() {
            return this;
        }
    }]);

    return List;
}();

exports.default = List;
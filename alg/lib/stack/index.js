"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/*
* 数据结构：栈
*/

var Stack = function () {
    function Stack() {
        _classCallCheck(this, Stack);

        this._datastore = [];
        this._top = 0;
    }

    _createClass(Stack, [{
        key: "push",
        value: function push(item) {
            this._datastore[this._top++] = item;
        }
    }, {
        key: "pop",
        value: function pop() {
            return this._datastore[--this._top];
        }
    }, {
        key: "peek",
        value: function peek() {
            return this._datastore[this._top - 1];
        }
    }, {
        key: "clear",
        value: function clear() {
            this._top = 0;
        }
    }, {
        key: "length",
        value: function length() {
            return this._top;
        }
    }]);

    return Stack;
}();

exports.default = Stack;
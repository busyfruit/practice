import Stack from './index';

/**
* 使用栈实现进制转换
* 将十进制数num转换为base进制的数
*/
function sysConvert(num, base) {
    let stack = new Stack();
    let ret = '';
    do {
        stack.push(num % base);
    }
    while(num = Math.floor(num / base));
    while(stack.length() > 0) {
        ret += stack.pop();
    }
    return ret;
}

console.log(sysConvert(100, 2));

/**
* 使用栈检测字符串是否为回文
*/
function isPlalindrome(str) {
    str = '' + str;
    let stack = new Stack();
    let ret = true;
    for (let i = 0; i < str.length; i ++) {
        stack.push(str[i]);
    }
    for (let i = 0; i < str.length; i ++) {
        if (str[i] !== stack.pop()) {
            ret = false;
            break;
        }
    }
    return ret;
}

console.log(isPlalindrome('racecar'));
console.log(isPlalindrome('abc'));

// 使用栈实现阶乘
function factorial(num) {
    let stack = new Stack();
    while (num > 0) {
        stack.push(num --);
    }
    let ret = 1;
    while (stack.length() > 0) {
        ret *= stack.pop();
    }
    return ret;
}

console.log(factorial(5));

/*
* 数据结构：栈
*/

class Stack {

    constructor() {
        this._datastore = [];
        this._top = 0;
    }

    push(item) {
        this._datastore[this._top ++] = item;
    }

    pop() {
        return this._datastore[-- this._top];
    }

    peek() {
        return this._datastore[this._top - 1];
    }

    clear() {
        this._top = 0;
    }

    length() {
        return this._top;
    }
}

export default Stack;

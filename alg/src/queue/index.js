/**
* 队列
*/

class Queue {
    constructor(size) {
        this._datastore = new Array(size + 1);
        this._size = size;
        this._head = 0;
        this._tail = 0;
    }

    enqueue(item) {
        let tail = this._tail + 1 > this._size ? 0 : this._tail + 1;
        // 队列上溢
        if (tail === this._head) {
            throw new Error('queue overflow');
        }
        this._datastore[this._tail] = item;
        this._tail = tail;
    }

    dequeue () {
        // 队列下溢
        if (this._tail === this._head) {
            throw new Error('queue underflow');
        }
        let ret = this._datastore[this._head];
        if (this._head === this._size) {
            this._head = 0;
        }
        else {
            this._head += 1;
        }
        return ret;
    }
}

export default Queue;

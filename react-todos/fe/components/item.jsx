var React = require('react');

module.exports = React.createClass({
	getInitialState: function () {
		return {
			data: this.props.data
		};
	},

	render: function () {
		var item = this.state.data;
		return (
			<li ref="wrapper" className={item.done ? 'done' : ''}>
				<div className="view" onDoubleClick={this.editing}>
					<input type="checkbox" checked={item.done} onChange={this.props.toggle} />
					<label>{item.title}</label>
					<a className="destroy" onClick={this.props.removeItem}></a>
				</div>
				<input 
					ref="input" 
					className="edit" 
					type="text"
					defaultValue={item.title} 
					onBlur={this.modify} 
					onKeyPress={this.modify}  />
			</li>
		);
	},

	editing: function () {
		var li = React.findDOMNode(this.refs.wrapper);
		li.className += ' editing';
	},

	modify: function (e) {
		if (e.type === 'keypress' && e.nativeEvent.keyCode !== 13) {
			return;
		}
		var input = React.findDOMNode(this.refs.input);
		if (input.value === '') {
			this.props.removeItem();
		} else {
			var item = this.state.data;
			item.title = input.value;
			this.setState({data: item});
			localStorage.setItem(item.id, JSON.stringify(item));
		}
		var li = React.findDOMNode(this.refs.wrapper);
		var className = li.className.split(' ');
		className.splice(className.indexOf('editing'), 1);
		li.className = className.join(' ');
	}
});
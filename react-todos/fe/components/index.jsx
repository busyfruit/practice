var React = require('react');
var Item = require('./item.js');

module.exports = React.createClass({
	getInitialState: function () {
		return {
			remain: 0,
			completed: 0,
			list: []
		};
	},

	componentDidMount: function () {
		var list = [];
		Object.keys(localStorage).forEach(function (key) {
			var value = localStorage.getItem(key);
			list.push(JSON.parse(value));
		});
		var remain = 0, completed = 0;
		list.forEach(function (item) {
			item.done ? completed ++ : remain ++;
		});
		this.setState({
			remain: remain,
			completed: completed,
			list: list
		});
	},

	addItem: function (e) {
		if (e.nativeEvent.keyCode === 13) {
			var input = React.findDOMNode(this.refs.input);
			if (input.value === '') {
				return false;
			}
			var item = {
				title: input.value,
				done: false,
				id: Date.now()
			};
			var list = this.state.list;
			list.push(item);
			this.setState({
				remain: this.state.remain + 1,
				list: list
			});
			input.value = '';
			localStorage.setItem(item.id, JSON.stringify(item));
		}
	},

	toggleAll: function () {
		var remain = !this.state.remain;
		var isAllDone = this.state.remain > 0;
		var list = this.state.list;
		var total = list.length;
		list.forEach(function (item) {
			item.done = isAllDone;
			localStorage.setItem(item.id, JSON.stringify(item));
		});
		this.setState({
			remain: isAllDone ? 0 : total,
			completed: isAllDone ? total : 0,
			list: list
		});
	},

	toggle: function (item) {
		var isDone = item.done = !item.done;
		var remain = this.state.remain;
		var completed = this.state.completed;
		if (isDone) {
			remain = remain - 1;
			completed = completed + 1;
		} else {
			remain = remain + 1;
			completed = completed - 1;
		}
		this.setState({
			remain: remain,
			completed: completed
		});
		localStorage.setItem(item.id, JSON.stringify(item));
	},

	removeItem: function (item) {
		var list = this.state.list;
		var index = list.indexOf(item);
		var remain = this.state.remain;
		var completed = this.state.completed;
		list.splice(index, 1);
		if (!item.done) {
			remain = remain - 1;
		} else {
			completed = completed - 1;
		}
		this.setState({
			remain: remain,
			completed: completed,
			list: list
		});
		localStorage.removeItem(item.id);
	},

	clearCompleted: function () {
		var list = this.state.list.filter(function (item) {
			if (item.done) {
				localStorage.removeItem(item.id);
			}
			return item.done === false;
		});
		this.setState({
			list: list,
			completed: 0,
			remain: this.state.remain
		});

	},

	render: function () {
		var listNodes = this.state.list.map(function (item) {
			return (
				<Item
					key={item.id}
					data={item}
					removeItem={this.removeItem.bind(this, item)}
					toggle={this.toggle.bind(this, item)} />
			);
		}, this);

		return (
			<div id="todoapp">
				<header>
					<div className="clearfix">
						<h1 className="fl-left">Todolist</h1>
						<div className="ucenter fl-right">
							<a href="/login" className="useRouter">登录</a>
							<a href="/signup" className="useRouter">注册</a>
						</div>
					</div>
					<input type="text" placeholder="What needs to be done?" ref="input" onKeyPress={this.addItem} />
				</header>
				<section id="main" className={this.state.list.length ? 'show' : 'hide'}>
					<input type="checkbox" id="toggle-all" checked={this.state.remain === 0} onChange={this.toggleAll} />
					<label htmlFor="toggle-all">Mark all as complete</label>
					<ul id="todo-list">
						{listNodes}
					</ul>
				</section>
				<footer className={this.state.list.length ? 'show' : 'hide'}>
					<a id="clear-completed" onClick={this.clearCompleted} className={this.state.completed ? 'show' : 'hide'}>
						Clear {this.state.completed} completed item
					</a>
					<div className="todo-count">
						<b>{this.state.remain}</b> item left
					</div>
				</footer>
			</div>
		);
	}
});
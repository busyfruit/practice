var React = require('react');
var Item = require('./item.js');

module.exports = React.createClass({
	render: function () {
		return (
			<form action="/api/login" method="post">
				<ul>
					<li><label>用户名：</label><input type="text" name="uname" /></li>
					<li><label>密码：</label><input type="password" name="pword" /></li>
					<li><label><input type="checkbox" name="persistent" />保持登录状态</label></li>
					<li><button type="submit">登录</button></li>
				</ul>
			</form>
		);
	}
});
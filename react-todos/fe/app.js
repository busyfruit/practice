var React = require('react');
var Router = require('./router/router.js');

// var Index = require('./components/index.js');


Router.set({
	'/': function () {
		require.ensure([], function (require) {
			var Index = require('./components/index.js');
			React.render(React.createElement(Index), document.querySelector('#content'));
		})
	},

	'/login': function () {
		require.ensure([], function (require) {
			var Login = require('./components/login.js');
			React.render(React.createElement(Login), document.querySelector('#content'));
		});
	},

	'/signup': function () {

	}
});

Router.start();

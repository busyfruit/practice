/**
* 基于history API实现的简单router
*/

var utils = {
	getURLParts: function (url) {
		var a = document.createElement('a');
		var obj = {};
		var parts = 'href|protocol|host|hostname|port|pathname|search|hash';
		a.href = url;
		parts.split('|').forEach(function (key) {
			obj[key] = a[key];
		});
		obj.relativeURL = a.href.replace(a.protocol + '//' + a.host, '');
		return obj;
	},

	search2Obj: function (search) {
		var obj = {};
		if (search) {
			search.substr(1).split('&').forEach(function (pairs) {
				pairs = pairs.split('=');
				obj[pairs[0]] = pairs[1];
			});
		}
		return obj;
	}
};



var router, currentPattern = null;

function bindHyperlinks () {
	document.body.addEventListener('click', function (e) {
		if (e.target.className.indexOf('useRouter') >= 0) {
			e.preventDefault();
			router && router.navigate(e.target.href);
		}
	}, false);
}

/**
* 将url映射成router的匹配模式，TODO
*/
function searchPattern (url) {
	return url;
}

function Router () {
	var mapping = this.mapping = {};
	window.addEventListener('popstate', function (e) {
		var urlParts = utils.getURLParts(location.href);
		var pattern = searchPattern(urlParts.relativeURL);
		mapping[pattern](utils.search2Obj(urlParts.search), e.state);
	}, false);
	bindHyperlinks();
}

Router.prototype.set = function (pattern, handler) {
	if (pattern.toString() === '[object Object]') {
		for (var i in pattern) {
			this.mapping[i] = pattern[i];
		}
	} else {
		this.mapping[pattern] = handler;
	}
}

Router.prototype.navigate = function (url, state) {
	var urlParts = utils.getURLParts(url);
	var pattern = searchPattern(urlParts.relativeURL);
	var queryObj = utils.search2Obj(urlParts.search);
	history.pushState(state || {}, '', url);
	this.mapping[pattern](queryObj, state);
	currentPattern = pattern;
	return true;
}

Router.prototype.start = function () {
	var urlParts = utils.getURLParts(location.href);
	var pattern = searchPattern(urlParts.relativeURL);
	if (currentPattern === pattern) return;
	currentPattern = pattern;
	this.mapping[currentPattern]();
}

module.exports = router || (router = new Router);